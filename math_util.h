#ifndef MATH_UTIL_H
#define MATH_UTIL_H

#include <math.h>
#include <stdbool.h>

#define EPS 0.00001f
#define PI 3.141592653589793f

bool flt_eq(float a, float b);

#endif
