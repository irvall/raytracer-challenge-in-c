#ifndef TUPLE_H
#define TUPLE_H
#include "math_util.h"
#include <stdbool.h>
#include <stdio.h>
typedef struct Tuple Tuple;
typedef struct Tuple Point;
typedef struct Tuple Color;

struct Tuple {
    float x;
    float y;
    float z;
    float w;
};

Tuple tuple_new(float x, float y, float z, float w);
Tuple tuple_negate(Tuple t);
Tuple tuple_add(Tuple t1, Tuple t2);
Tuple tuple_sub(Tuple t1, Tuple t2);
Tuple tuple_mul_scalar(Tuple t, float scalar);
bool tuple_eq(Tuple t1, Tuple t2);
bool tuple_is_vector(Tuple);
void tuple_print(Tuple t);

#endif
