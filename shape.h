#ifndef SHAPE_H
#define SHAPE_H
#include "point.h"
#include "matrix.h"

struct Sphere {
    float radius;
    Point location;
    Matrix transform;
};

typedef struct Sphere Sphere;

Sphere sphere();
bool sphere_eq(Sphere, Sphere);
void sphere_set_transform(Sphere *s, Matrix m);


#endif