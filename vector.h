#ifndef VECTOR_H
#define VECTOR_H
#include "tuple.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct Tuple Vector;

Vector vector(float x, float y, float z);
Vector vector_normalize(Vector v);
Vector vector_cross(Vector v, Vector w);
Vector vector_div_scalar(Vector v, float scalar);
float vector_magnitude(Vector v);
float vector_dot(Vector v, Vector w);
void vector_print(Vector v);
bool vector_eq(Vector v, Vector w);

#endif
