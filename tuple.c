#include "tuple.h"

Tuple tuple_new(float x, float y, float z, float w) {
    Tuple  t;
    t.x =  x;
    t.y =  y;
    t.z =  z;
    t.w =  w;
    return t;
}

Tuple tuple_negate(Tuple t) {
    return tuple_new(-t.x, -t.y, -t.z, -t.w);
}

Tuple tuple_add(Tuple t1, Tuple t2) {
    return tuple_new(t1.x+t2.x, t1.y+t2.y, t1.z+t2.z, t1.w+t2.w);
}

Tuple tuple_sub(Tuple t1, Tuple t2) {
    return tuple_new(t1.x-t2.x, t1.y-t2.y, t1.z-t2.z, t1.w-t2.w);
}

Tuple tuple_mul_scalar(Tuple t, float scalar) {
    return tuple_new(t.x*scalar, t.y*scalar, t.z*scalar, t.w*scalar);

}


bool tuple_eq(Tuple t1, Tuple t2) {
    return flt_eq(t1.x, t2.x) &&
           flt_eq(t1.y, t2.y) &&
           flt_eq(t1.z, t2.z) &&
           flt_eq(t1.w, t2.w);
}

bool tuple_is_vector(Tuple t) {
    return flt_eq(t.w, 0.0f);
}

void tuple_print(Tuple t) {
    printf("tuple { %f, %f, %f, %f }\n", t.x, t.y, t.z, t.w);
}

