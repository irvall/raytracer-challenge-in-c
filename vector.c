#include "vector.h"

Vector vector(float x, float y, float z) {
    Vector v;
    v.x = x;
    v.y = y;
    v.z = z;
    v.w = 0;
    return v;
}

Vector vector_normalize(Vector v) {
    float mag = vector_magnitude(v);
    return vector(v.x/mag, v.y/mag, v.z/mag);
}


float vector_magnitude(Vector v) {
    return sqrtf(v.x*v.x+v.y*v.y+v.z*v.z);
}


float vector_dot(Vector v, Vector w) {
    return v.x*w.x+v.y*w.y+v.z*w.z;
}

Vector vector_cross(Vector v, Vector w) {
    float cx = v.y*w.z - v.z*w.y;
    float cy = v.z*w.x - v.x*w.z;
    float cz = v.x*w.y - v.y*w.x;
    return vector(cx, cy, cz);
}

Vector vector_div_scalar(Vector v, float s)
{
    return vector(v.x/s, v.y/s, v.z/s);
}

void vector_print(Vector v) {
    printf("Vector { %f, %f, %f }", v.x, v.y, v.z);
}


bool vector_eq(Vector v, Vector w)
{
    return flt_eq(v.x, w.x)
        && flt_eq(v.y, w.y) 
        && flt_eq(v.z, w.z);
}

