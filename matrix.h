#ifndef MATRIX_H
#define MATRIX_H
#include <stdbool.h>
#include <stdarg.h>
#include "tuple.h"
#include "math_util.h"

typedef struct Matrix {
    int rows;
    int cols;
    float *data;
} Matrix;

Matrix matrix_from_ints(int rows, int cols, int size, ...);
Matrix matrix_from_floats(int rows, int cols, int size, ...);
Matrix matrix_new(int rows, int cols);
Matrix matrix_identity(int rows, int cols);
Matrix matrix_add(Matrix *m1, Matrix *m2);
Matrix matrix_mul(Matrix *m1, Matrix *m2);
Matrix matrix_transpose(Matrix *m);
Matrix matrix_submatrix(Matrix *m, int row, int column);
Matrix matrix_inverse(Matrix *m);
Matrix matrix_translate(float x, float y, float z);
Matrix matrix_scale(float x, float y, float z);
Matrix matrix_rotatex(float radius);
Matrix matrix_rotatey(float radius);
Matrix matrix_rotatez(float radius);
Matrix matrix_shear(float xy, float xz, float yx, float yz, float zx, float zy);
Tuple matrix_mul_tuple(Matrix *m, Tuple t);
Point matrix_mul_point(Matrix *m, Point p);
float matrix_determinant(Matrix *m);
float matrix_minor(Matrix *m, int row, int column);
float matrix_cofactor(Matrix *m, int row, int column);
float matrix_get(Matrix *m, int x, int y);
float dot_product(int size, float *xs, float *ys);
bool matrix_eq(Matrix *m1, Matrix *m2);
bool matrix_invertible(Matrix *m);
void matrix_print(Matrix *m);
void matrix_set(Matrix *m, int x, int y, float value);

#endif
