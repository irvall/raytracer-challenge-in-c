#include "color.h"

const Color WHITE = { 1.0f, 1.0f, 1.0f, 0.0f };
const Color BLACK = { 0.0f, 0.0f, 0.0f, 0.0f };
const Color RED   = { 1.0f, 0.0f, 0.0f, 0.0f };
const Color GREEN = { 0.0f, 1.0f, 0.0f, 0.0f };
const Color BLUE  = { 0.0f, 0.0f, 1.0f, 0.0f };


Color color(int r, int g, int b)
{
    Color c;
    c.x = r/255.0f;
    c.y = g/255.0f;
    c.z = b/255.0f;
    c.w = 0.0f;
    return c;
}
void color_print(Color color) {
    printf("%d %d %d", (int)(color.x*255.999f), (int)(color.y*255.999f), (int)(color.z*255.999f));
}
