#ifndef RAY_H 
#define RAY_H
#include "point.h"
#include "vector.h"
#include "shape.h"
#include "matrix.h"
#include <stdarg.h>


#define NO_HIT (Intersection){ .t = -666.0f, .object = ((Sphere){ .radius = 0.0f, .location = ((Point){0,0,0,1})})}

struct Ray {
    Point origin;
    Vector direction;
};

struct Intersection {
    float t;
    Sphere object;
};

struct Intersections {
    int hitCount;
    struct Intersection* intersection;
};

typedef struct Ray Ray;
typedef struct Intersections Intersections;
typedef struct Intersection Intersection;

Ray ray(Point origin, Vector direction);
Ray ray_transform(Ray r, Matrix m);

Point ray_position(Ray, float tick);
Intersections ray_intersect(Sphere, Ray);
Intersections intersections(int, ...);
Intersection intersection(float t, Sphere sphere);
Intersection ray_hit(Intersections iss);
bool intersection_eq(Intersection, Intersection);

void ray_print(Ray);
void intersection_print(Intersection);
void intersections_print(Intersections);
#endif
