#include "point.h"
#include "vector.h"

Point point(float x, float y, float z) 
{
    Point p;
    p.x = x;
    p.y = y;
    p.z = z;
    p.w = 1;
    return p;
}

Point add(Point a, Point b) 
{
    return point(a.x + b.x, a.y + b.y, a.z + b.z);
}


bool point_eq(Point a, Point b)
{
    return flt_eq(a.x, b.x)
        && flt_eq(a.y, b.y)
        && flt_eq(a.z, b.z);
}


void point_print(Point p)
{
    printf("(%f, %f, %f)\n", p.x, p.y, p.z);
}
