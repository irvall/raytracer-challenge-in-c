#ifndef TEST_H
#define TEST_H
#include "color.h"
#include "vector.h"
#include "point.h"
#include "matrix.h"
#include "ray.h"
#include <assert.h>
bool run_tests(bool debug);
#endif
