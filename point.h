#ifndef POINT_H 
#define POINT_H
#include "tuple.h"

Point point(float x, float y, float z);
bool point_eq(Point a, Point b);
void point_print(Point p);
#endif
