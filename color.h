#ifndef COLOR_H
#define COLOR_H

#include "tuple.h"
#include <stdio.h>

typedef struct Tuple Color;

const Color WHITE;
const Color BLACK;
const Color RED;
const Color GREEN;
const Color BLUE;

Color color(int r, int g, int b);
void color_print(Color);

#endif
