#include "ray.h"
#include "tuple.h"

Ray ray(Point origin, Vector direction)
{
    Ray r;
    r.origin = origin;
    r.direction = direction;
    return r;
}

Ray ray_transform(Ray r, Matrix m)
{
    Point p  = matrix_mul_tuple(&m, r.origin);
    Vector v = matrix_mul_tuple(&m, r.direction);
    return ray(p, v);
}


Point ray_position(Ray ray, float tick)
{
    float px = tick * ray.direction.x + ray.origin.x;
    float py = tick * ray.direction.y + ray.origin.y;
    float pz = tick * ray.direction.z + ray.origin.z;
    return point(px, py, pz);
}

Intersections ray_intersect(Sphere sphere, Ray ray)
{
    ray = ray_transform(ray, matrix_inverse(&sphere.transform));
    Vector sphere_to_ray = tuple_sub(ray.origin, sphere.location);
    float a = vector_dot(ray.direction, ray.direction);
    float b = 2 * vector_dot(ray.direction, sphere_to_ray);
    float c = vector_dot(sphere_to_ray, sphere_to_ray) - 1.0f;
    float discriminant = b * b - (4.0f * a * c);
    // printf("a %f, b %f, c %f, dscr %f\n", a, b, c, discriminant);
    if (discriminant < 0)
    {
        return (Intersections){.hitCount = 0, .intersection = 0};
    }
    float dsqr = sqrtf(discriminant);
    float t1 = (-b - dsqr) / (2 * a);
    float t2 = (-b + dsqr) / (2 * a);
    Intersection i1 = (Intersection){.t = t1, .object = sphere};
    Intersection i2 = (Intersection){.t = t2, .object = sphere};
    return intersections(2, i1, i2);
}

Intersection intersection(float t, Sphere sphere)
{
    return (Intersection){.object = sphere, .t = t};
}

Intersections intersections(int hitCount, ...)
{
    va_list is;
    va_start(is, hitCount);
    Intersection *iss = (Intersection *)malloc(hitCount * sizeof(Intersection));
    for (int i = 0; i < hitCount; i++)
    {
        Intersection arg = va_arg(is, Intersection);
        iss[i] = arg;
    }

    return (Intersections){.hitCount = hitCount, .intersection = iss};
}

bool intersection_eq(Intersection i1, Intersection i2)
{
    return flt_eq(i1.t, i2.t) && sphere_eq(i1.object, i2.object);
}

int compare(const void *s1, const void *s2)
{
    Intersection *i1 = (Intersection *)s1;
    Intersection *i2 = (Intersection *)s2;
    if (i1->t < i2->t)
        return -1;
    else if (i1->t > i2->t)
        return 1;
    return 0;
}

Intersection ray_hit(Intersections iss)
{
    qsort(iss.intersection, iss.hitCount, sizeof(Intersection), compare);
    int i = 0;
    int j = iss.hitCount - 1;
    Intersection lowestHit = iss.intersection[0];
    if (lowestHit.t > 0)
        return lowestHit;
    while (i < j - 1)
    {
        int m = ((j - i) / 2) + i;
        Intersection low = iss.intersection[i];
        if (low.t > 0)
            return low;

        Intersection mid = iss.intersection[m];
        if (mid.t < 0)
        {
            i = m;
            continue;
        }
        if (mid.t > 0)
        {
            j = m;
            continue;
        }
    }
    Intersection hi = iss.intersection[j];
    if(hi.t < 0)
        return NO_HIT;
    Intersection lo = iss.intersection[i];
    return lo.t > 0 ? lo : hi;
}

// qsort(data, count, sizeof(struct employee), compare);

void intersection_print(Intersection is)
{
    printf("intersection t=%f\n", is.t);
}

void intersections_print(Intersections iss)
{
    for (int i = 0; i < iss.hitCount; i++)
    {
        intersection_print(iss.intersection[i]);
    }
}

void ray_print(Ray r)
{
    printf("ray(origin=(%f,%f,%f), direction=(%f,%f,%f))\n", r.origin.x, r.origin.y, r.origin.z, r.direction.x, r.direction.y, r.direction.z);
}
