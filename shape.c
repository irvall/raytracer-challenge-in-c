#include "shape.h"

Sphere sphere()
{
    Sphere s;
    s.location = point(0,0,0);
    s.radius = 1;
    s.transform = matrix_identity(4, 4);
    return s;
}


bool sphere_eq(Sphere a, Sphere b)
{
    return point_eq(a.location, b.location) && flt_eq(a.radius, b.radius);
}

void sphere_set_transform(Sphere *s, Matrix transform)
{
    s->transform = transform;
}

