#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "matrix.h"
#include "point.h"

Matrix matrix_from_ints(int rows, int cols, int size, ...)
{
    va_list ap;
    va_start(ap, size);
    Matrix mat = matrix_new(rows, cols);
    for (int i = 0; i < size; i++)
    {
        int value = va_arg(ap, int);
        mat.data[i] = (float)value;
    }
    va_end(ap);
    return mat;
}

Matrix matrix_from_floats(int rows, int cols, int size, ...)
{
    va_list ap;
    va_start(ap, size);
    Matrix mat = matrix_new(rows, cols);
    for (int i = 0; i < size; i++)
    {
        float value = va_arg(ap, double);
        mat.data[i] = value;
    }
    va_end(ap);
    return mat;
}


Matrix matrix_new(int rows, int cols)
{
    Matrix mat;
    mat.rows = rows;
    mat.cols = cols;
    mat.data = malloc(cols * rows * sizeof(float));
    for (int j = 0; j < cols; j++)
    {
        for (int i = 0; i < rows; i++)
        {
            matrix_set(&mat, i, j, 0.0f);
        }
    }
    return mat;
}

Matrix matrix_identity(int rows, int cols)
{
    Matrix mat;
    mat.rows = rows;
    mat.cols = cols;
    mat.data = malloc(cols * rows * sizeof(float));
    for(int i = 0; i < rows; i++)
    {
        matrix_set(&mat, i, i, 1);
    }
    return mat;
}

Matrix matrix_translate(float x, float y, float z)
{
    Matrix res = matrix_identity(4, 4);
    matrix_set(&res, 3, 0, x);
    matrix_set(&res, 3, 1, y);
    matrix_set(&res, 3, 2, z);
    return res;
}


Matrix matrix_scale(float x, float y, float z)
{
    Matrix scale = matrix_identity(4, 4);
    matrix_set(&scale, 0, 0, x);
    matrix_set(&scale, 1, 1, y);
    matrix_set(&scale, 2, 2, z);
    return scale;
}

Matrix matrix_rotatex(float radians)
{
    Matrix rotate = matrix_identity(4, 4);
    float c = cosf(radians);
    float s = sinf(radians);
    matrix_set(&rotate, 1, 1, c);
    matrix_set(&rotate, 2, 2, c);
    matrix_set(&rotate, 1, 2, s);
    matrix_set(&rotate, 2, 1, -s);
    return rotate;

}

Matrix matrix_rotatey(float radians)
{
    Matrix rotate = matrix_identity(4, 4);
    float c = cosf(radians);
    float s = sinf(radians);
    matrix_set(&rotate, 0, 0, c);
    matrix_set(&rotate, 2, 2, c);
    matrix_set(&rotate, 2, 0, s);
    matrix_set(&rotate, 0, 2, -s);
    return rotate;
}

Matrix matrix_rotatez(float radians)
{
    Matrix rotate = matrix_identity(4, 4);
    float c = cosf(radians);
    float s = sinf(radians);
    matrix_set(&rotate, 0, 0, c);
    matrix_set(&rotate, 1, 1, c);
    matrix_set(&rotate, 1, 0, -s);
    matrix_set(&rotate, 0, 1, s);
    return rotate;
}

Matrix matrix_shear(float xy, float xz, float yx, float yz, float zx, float zy)
{
    Matrix shear = matrix_identity(4, 4);
    matrix_set(&shear, 1, 0, xy);
    matrix_set(&shear, 2, 0, xz);
    matrix_set(&shear, 0, 1, yx);
    matrix_set(&shear, 0, 2, zx);
    matrix_set(&shear, 2, 1, yz);
    matrix_set(&shear, 1, 2, zy);
    return shear;
}

void matrix_set(Matrix *m, int x, int y, float value)
{
    m->data[y * m->cols + x] = value;
}

float matrix_get(Matrix *m, int x, int y)
{
    return m->data[y * m->rows + x];
}

Matrix matrix_add(Matrix *m1, Matrix *m2)
{
    assert(m1->rows == m2->rows);
    assert(m1->cols == m2->cols);
    Matrix res = matrix_new(m1->rows, m1->cols);
    for (int j = 0; j < m1->cols; j++)
    {
        for (int i = 0; i < m1->rows; i++)
        {
            matrix_set(&res, i, j, matrix_get(m1, i, j) + matrix_get(m2, i, j));
        }
    }
    return res;
}

Matrix matrix_mul(Matrix *m1, Matrix *m2)
{
    assert(m1->rows == m2->cols);
    Matrix res = matrix_new(m1->rows, m2->cols);
    for (int j = 0; j < m1->cols; j++)
    {
        float *xs = malloc(m1->rows * sizeof(float));
        for (int k = 0; k < m1->rows; k++)
        {
            xs[k] = matrix_get(m1, k, j);
        }
        for (int i = 0; i < m2->rows; i++)
        {
            float *ys = malloc(m2->cols * sizeof(float));
            for (int k = 0; k < m2->cols; k++)
            {
                ys[k] = matrix_get(m2, i, k);
            }
            float dot = dot_product(m1->rows, xs, ys);
            matrix_set(&res, i, j, dot);
        }
    }
    return res;
}

float dot_product(int n, float *xs, float *ys)
{
    float res = 0.0f;
    for (int i = 0; i < n; i++)
    {
        res += xs[i] * ys[i];
    }
    return res;
}

Matrix matrix_transpose(Matrix *m)
{
    Matrix res = matrix_new(m->rows, m->cols);

    for (int j = 0; j < m->cols; j++)
    {
        for (int i = 0; i < m->rows; i++)
        {
            float value = matrix_get(m, j, i);
            matrix_set(&res, i, j, value);
        }
    }

    return res;
}

Matrix matrix_submatrix(Matrix *m, int row, int column)
{
    int new_rows = m->rows - 1;
    int new_cols = m->cols - 1;
    Matrix sub = matrix_new(new_rows, new_cols);
    int index = 0;
    for (int j = 0; j < m->cols; j++)
    {
        if(j == column) continue;
        for (int i = 0; i < m->rows; i++)
        {
            if(i == row) continue;
            float value = matrix_get(m, i, j);
            sub.data[index++] = value;
        }
    }
    return sub;
}


Matrix matrix_inverse(Matrix *m)
{
    float det = matrix_determinant(m);
    if(!det) {
        fprintf(stderr, "%s\n", "matrix determinant is zero; no inverse");
        return *m;
    }
    Matrix co = matrix_new(m->rows, m->cols);
    for(int j = 0; j < m->cols; j++)
    {
        for(int i = 0; i < m->rows; i++)
        {
            float cf = matrix_cofactor(m, i, j);
            matrix_set(&co, i, j, cf);
        }
    }
    Matrix coT = matrix_transpose(&co);
    float divBy = 1/det;
    for(int j = 0; j < m->cols; j++)
    {
        for(int i = 0; i < m->rows; i++)
        {
            float updated = matrix_get(&coT, i, j) * divBy;
            matrix_set(&coT, i, j, updated);
        }
    }
    return coT;
}

float matrix_determinant(Matrix *m)
{ 
    if(m->cols > 2) {
        float det = 0.0f;
        for(int i = 0; i < m->cols; i++) {
            float a = matrix_get(m, i, 0);
            float b = matrix_cofactor(m, i, 0);
            det += a*b;
        }
        return det;
    }
    float a = matrix_get(m, 0, 0);
    float b = matrix_get(m, 1, 0);
    float c = matrix_get(m, 0, 1);
    float d = matrix_get(m, 1, 1);
    return (a * d) - (b * c);
}

float matrix_minor(Matrix *m, int row, int column)
{
    Matrix sub = matrix_submatrix(m, row, column);
    return matrix_determinant(&sub);
}

float matrix_cofactor(Matrix *m, int row, int column)
{
    float res = matrix_minor(m, row, column);
    return (row+column) % 2 == 0 ? res : -res;
}

Point matrix_mul_point(Matrix *m, Point p)
{
    Point result = point(0, 0, 0);
    
    for (int i = 0; i < 3; i++)
    {
        float a = matrix_get(m, 0, i);
        float b = matrix_get(m, 1, i);
        float c = matrix_get(m, 2, i);

        if (i == 0)
        {
            result.x = a * p.x + b * p.y + c * p.z;
        }
        else if (i == 1)
        {
            result.y = a * p.x + b * p.y + c * p.z;
        }
        else if (i == 2)
        {
            result.z = a * p.x + b * p.y + c * p.z;
        }
    }

    return result;
}

Tuple matrix_mul_tuple(Matrix *m, Tuple p)
{
    Point result = point(0, 0, 0);

    for (int i = 0; i < 4; i++)
    {
        float a = matrix_get(m, 0, i);
        float b = matrix_get(m, 1, i);
        float c = matrix_get(m, 2, i);
        float d = matrix_get(m, 3, i);

        if (i == 0)
        {
            result.x = a * p.x + b * p.y + c * p.z + d * p.w;
        }
        else if (i == 1)
        {
            result.y = a * p.x + b * p.y + c * p.z + d * p.w;
        }
        else if (i == 2)
        {
            result.z = a * p.x + b * p.y + c * p.z + d * p.w;
        }
        else if (i == 3)
        {
            result.w = a * p.x + b * p.y + c * p.z + d * p.w;
        }
 
    }

    return result;
}



bool matrix_eq(Matrix *m1, Matrix *m2)
{
    if (m1->rows != m2->rows || m1->cols != m2->cols)
        return false;
    for (int j = 0; j < m1->cols; j++)
    {
        for (int i = 0; i < m1->rows; i++)
        {
            if (!flt_eq(matrix_get(m1, i, j), matrix_get(m2, i, j)))
                return false;
        }
    }
    return true;
}


bool matrix_invertible(Matrix *m) 
{
    return matrix_determinant(m);
}

void matrix_print(Matrix *mat)
{
    for (int j = 0; j < mat->cols; j++)
    {
        printf("|");
        for (int i = 0; i < mat->rows; i++)
        {
            printf(" %f\t|", matrix_get(mat, i, j));
        }
        printf("\n");
    }
    printf("\n");
}
