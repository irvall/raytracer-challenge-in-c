#include "test.h"

struct Canvas
{
  int width;
  int height;
  size_t total_pixels;
  Color *pixels;
};

typedef struct Canvas Canvas;

Color globalColor;

Canvas canvas(int width, int height)
{
  globalColor = BLACK;
  size_t total_pixels = width * height;
  Color *pixels = (Color *)malloc(total_pixels * sizeof(Color));
  for (size_t i = 0; i < total_pixels; i++)
  {
    pixels[i] = globalColor;
  }
  Canvas result = {width, height, total_pixels, pixels};
  return result;
}

void print_canvas(Canvas canvas)
{
  printf("P3\n");
  printf("%d %d\n", canvas.width, canvas.height);
  printf("255\n");
  for (int y = 0; y < canvas.height; y++)
  {
    for (int x = 0; x < canvas.width; x++)
    {
      color_print(canvas.pixels[y * canvas.width + x]);
      printf(" ");
    }
    printf("\n");
  }
}

void canvas_set_pixel(Canvas canvas, int x, int y)
{
  if (x < 0 || y < 0)
    return;
  int index = (y * canvas.height) + x;
  if (index < 0 || (size_t)index > canvas.total_pixels)
    return;
  canvas.pixels[index] = globalColor;
}

void draw_point(Canvas canvas, Point p, Color color)
{
  Color oldGlobal = globalColor;
  globalColor = color;
  p.y = -p.y;
  int size = 2;
  int xt = p.x + 256;
  int yt = p.y + 256;
  for (int y = yt - size; y <= yt + size; y++)
    for (int x = xt - size; x <= xt + size; x++)
      canvas_set_pixel(canvas, x, y);
  globalColor = oldGlobal;
}

void draw_line(Canvas canvas, int x1, int x2, int y1, int y2)
{
  for (int y = y1; y <= y2; y++)
    for (int x = x1; x <= x2; x++)
      canvas_set_pixel(canvas, x, y);
}

void test_drawing()
{
  int w = 512;
  int h = 512;
  int unit = 100;
  Canvas coord_sys = canvas(w, h);
  Color colors[3] = {RED, GREEN, BLUE};
  Point o = point(0, 2 * unit, 0);
  int n = 256;
  float rad = (2.0f * PI) / (float)n;
  for (int i = 0; i < n; i++)
  {
    Matrix r = matrix_rotatez(rad * i);
    Point p = matrix_mul_tuple(&r, o);
    draw_point(coord_sys, p, colors[i % 3]);
  }
  print_canvas(coord_sys);
}

int main()
{
  //run_tests(false);
  Canvas canv = canvas(512, 512);
  int k = 10;
  int m = 10;
  float pixelSize = 1.0f;
  float d = 1.0f;
  Sphere s = sphere();
  Point eye = point(-5, 0, 0);
  Vector up = vector(0, 1, 0);
  Vector tArrow = tuple_sub(s.location, eye);
  Vector bArrow = vector_cross(tArrow, up);
  Vector tn = vector_div_scalar(tArrow, vector_magnitude(tArrow));
  Vector bn = vector_div_scalar(bArrow, vector_magnitude(bArrow));
  Vector vn = vector_cross(tn, bn);
  Point C = tuple_add(eye, tuple_mul_scalar(tn, d));
  float gx = 512 / 2;
  float gy = 512 / 2;
  
  Vector qx = tuple_mul_scalar(bn, (2*gx)/(k-1)); 
  Vector qy = tuple_mul_scalar(vn, (2*gy)/(m-1)); 
  Vector c1 = tuple_mul_scalar(tn, d);
  Vector c2 = tuple_mul_scalar(bn, gx);
  Vector c3 = tuple_mul_scalar(vn, gy);
  Vector p1m = tuple_sub(c1, tuple_sub(c2, c3));
  for(int i = 0; i < k; i++) {
    for(int j = 0; j < m; j++) {
      Vector d1 = tuple_mul_scalar(qx, i);
      Vector d2 = tuple_mul_scalar(qy, j);
      Vector pij = tuple_add(p1m, tuple_add(d1, d2));
      Ray rij = ray(eye, vector_div_scalar(pij, vector_magnitude(pij)));
      Intersections iss = ray_intersect(s, rij);
      intersections_print(iss);

    }
  }
  point_print(C);




  return 0;
}
