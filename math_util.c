#include "math_util.h"

bool flt_eq(float a, float b)
{
    return fabsf(a - b) < EPS;
}