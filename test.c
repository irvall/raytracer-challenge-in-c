#include "test.h"

void test_matrix_mul(bool debug)
{
    if (debug)
        printf("\n# Matrix Multiplication Test\n");
    Matrix A = matrix_from_ints(4, 4, 16, 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2);
    if (debug)
        matrix_print(&A);

    Matrix B = matrix_from_ints(4, 4, 16, -2, 1, 2, 3, 3, 2, 1, -1, 4, 3, 6, 5, 1, 2, 7, 8);
    if (debug)
        matrix_print(&B);

    Matrix result = matrix_from_ints(4, 4, 16, 20, 22, 50, 48, 44, 54, 114, 108, 40, 58, 110, 102, 16, 26, 46, 42);
    if (debug)
        matrix_print(&result);

    Matrix multiplied = matrix_mul(&A, &B);
    if (debug)
        matrix_print(&multiplied);

    assert(matrix_eq(&multiplied, &result));
}

void test_matrix_mul_tuple(bool debug)
{
    if (debug)
        printf("\n# Matrix Multiply With Tuple Gives New Tuple\n");
    Matrix A = matrix_from_ints(4, 4, 16, 1, 2, 3, 4, 2, 4, 4, 2, 8, 6, 4, 1, 0, 0, 0, 1);
    if (debug)
        matrix_print(&A);
    Tuple b = tuple_new(1, 2, 3, 1);
    if (debug)
        tuple_print(b);
    Tuple expected = tuple_new(18, 24, 33, 1);
    if (debug)
        tuple_print(expected);
    Tuple result = matrix_mul_tuple(&A, b);
    if (debug)
        tuple_print(result);
    assert(tuple_eq(result, expected));
}

void test_matrix_transpose(bool debug)
{
    if (debug)
        printf("\n# Matrix Transpose Test\n");
    Matrix A = matrix_from_ints(4, 4, 16, 0, 9, 3, 0, 9, 8, 0, 8, 1, 8, 5, 3, 0, 0, 5, 8);
    Matrix expected = matrix_from_ints(4, 4, 16, 0, 9, 1, 0, 9, 8, 8, 0, 3, 0, 5, 5, 0, 8, 3, 8);
    Matrix tA = matrix_transpose(&A);
    if (debug)
        matrix_print(&A);
    if (debug)
        matrix_print(&tA);
    assert(matrix_eq(&tA, &expected));
}

void test_matrix_transpose_identity(bool debug)
{
    if (debug)
        printf("\n# Matrix Transpose Identity Test\n");
    Matrix I = matrix_from_ints(4, 4, 16, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    Matrix tI = matrix_transpose(&I);
    assert(matrix_eq(&I, &tI));
}

void test_matrix_determinant(bool debug)
{
    if (debug)
        printf("\n# Matrix Determinant Test\n");
    Matrix A = matrix_from_ints(2, 2, 4, 1, 5, -3, 2);
    float determinant = matrix_determinant(&A);
    assert(flt_eq(determinant, 17));
}

void test_matrix_submatrix(bool debug)
{
    Matrix m = matrix_from_ints(3, 3, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    Matrix sub1 = matrix_submatrix(&m, 0, 0);
    Matrix sub2 = matrix_submatrix(&m, 1, 1);
    Matrix sub3 = matrix_submatrix(&m, 0, 2);
    Matrix sub4 = matrix_submatrix(&m, 1, 2);
    Matrix sub5 = matrix_submatrix(&m, 0, 1);
    Matrix sub6 = matrix_submatrix(&m, 2, 0);
    if (debug)
        matrix_print(&m);
    if (debug)
        matrix_print(&sub1);
    if (debug)
        matrix_print(&sub2);
    if (debug)
        matrix_print(&sub3);
    if (debug)
        matrix_print(&sub4);
    if (debug)
        matrix_print(&sub5);
    if (debug)
        matrix_print(&sub6);

    Matrix m1 = matrix_from_ints(4, 4, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 7, 6, 5, 4, 3, 2, 1);
    Matrix sub7 = matrix_submatrix(&m1, 0, 0);
    Matrix sub8 = matrix_submatrix(&m1, 2, 2);
    Matrix sub9 = matrix_submatrix(&sub8, 0, 0);
    if (debug)
        matrix_print(&m1);
    if (debug)
        matrix_print(&sub7);
    if (debug)
        matrix_print(&sub8);
    if (debug)
        matrix_print(&sub9);
    Matrix exp = matrix_from_ints(2, 2, 4, 5, 7, 3, 1);
    assert(matrix_eq(&exp, &sub9));
}

void test_minor(bool debug)
{
    Matrix A = matrix_from_ints(3, 3, 9, 3, 5, 0, 2, -1, -7, 6, -1, 5);
    Matrix B = matrix_submatrix(&A, 1, 0);
    if (debug)
        matrix_print(&A);
    if (debug)
        matrix_print(&B);
    if (debug)
        printf("determinant of B: %f\n", matrix_determinant(&B));

     // TODO
}

void test_cofactor(bool debug)
{
    Matrix A = matrix_from_ints(3, 3, 9, 3, 5, 0, 2, -1, -7, 6, -1, 5);
    if (debug)
        matrix_print(&A);
    assert(matrix_minor(&A, 0, 0) == -12);
    assert(matrix_cofactor(&A, 0, 0) == -12);
    assert(matrix_minor(&A, 0, 1) == 25);
    assert(matrix_cofactor(&A, 0, 1) == -25);
    
}

void test_cofactor_and_determinant(bool debug)
{
    Matrix A = matrix_from_ints(4, 4, 16, -2, -8, 3, 5, -3, 1, 7, 3, 1, 2, -9, 6, -6, 7, 7, -9);
    if (debug)
        matrix_print(&A);
    assert(matrix_cofactor(&A, 0, 0) == 690);
    assert(matrix_cofactor(&A, 1, 0) == 447);
    assert(matrix_cofactor(&A, 2, 0) == 210);
    assert(matrix_cofactor(&A, 3, 0) == 51);
    assert(matrix_determinant(&A) == -4071);
    
}

void test_invertible(bool debug)
{
    Matrix A = matrix_from_ints(4, 4, 16, 6, 4, 4, 4, 5, 5, 7, 6, 4, -9, 3, -7, 9, 1, 7, -6);
    if (debug)
        matrix_print(&A);
    float detA = matrix_determinant(&A);
    assert(detA && detA == -2120);

    Matrix B = matrix_from_ints(4, 4, 16, -4, 2, -2, -3, 9, 6, 2, 6, 0, -5, 1, -5, 0, 0, 0, 0);
    if (debug)
        matrix_print(&B);
    float detB = matrix_determinant(&B);
    assert(!detB && detB == 0);
    
}

void test_inverse(bool debug)
{
    Matrix A = matrix_from_ints(4, 4, 16, -5, 2, 6, -8, 1, -5, 1, 8, 7, 7, -6, -7, 1, -3, 7, 4);
    Matrix B = matrix_inverse(&A);
    Matrix B_expected = matrix_from_floats(4, 4, 16,
                                           0.21805f, 0.45113f, 0.24060f, -0.04511f,
                                           -0.80827f, -1.45677f, -0.44361f, 0.52068f,
                                           -0.07895f, -0.22368f, -0.05263f, 0.19737f,
                                           -0.52256f, -0.81391f, -0.30075f, 0.30639f);
    if (debug)
        matrix_print(&A);
    if (debug)
        matrix_print(&B);
    if (debug)
        matrix_print(&B_expected);
    assert(flt_eq(matrix_determinant(&A), 532));
    assert(flt_eq(matrix_cofactor(&A, 3, 2), -160));
    assert(flt_eq(matrix_cofactor(&A, 2, 3), 105));
    assert(flt_eq(matrix_get(&B, 2, 3), -160.0 / 532.0));
    assert(flt_eq(matrix_get(&B, 3, 2), 105.0 / 532.0));
    assert(matrix_eq(&B, &B_expected));
    
}

void test_matrix_mul_product_by_its_inverse(bool debug)
{
    Matrix A = matrix_from_ints(4, 4, 16,
                                3, -9, 7, 3,
                                3, -8, 2, -9,
                                -4, 4, 4, 1,
                                -6, 5, -1, 1);
    Matrix B = matrix_from_ints(4, 4, 16,
                                8, 2, 2, 2,
                                3, -1, 7, 0,
                                7, 0, 5, 4,
                                6, -2, 0, 5);
    Matrix C = matrix_mul(&A, &B);
    Matrix Binv = matrix_inverse(&B);
    Matrix expectA = matrix_mul(&C, &Binv);
    if (debug)
        matrix_print(&A);
    if (debug)
        matrix_print(&expectA);
    assert(matrix_eq(&expectA, &A));
    
}

void test_matrix_translate_point(bool debug)
{
    Matrix transform = matrix_translate(5, -3, 2);
    Point p = point(-3, 4, 5);
    Point a = matrix_mul_tuple(&transform, p);
    Point b = point(2, 1, 7);
    assert(point_eq(a, b));
    
}

void test_matrix_inversion_translate(bool debug)
{
    Matrix transform = matrix_translate(5, -3, 2);
    Matrix inverse = matrix_inverse(&transform);
    Point p = point(-3, 4, 5);
    Point a = matrix_mul_tuple(&inverse, p);
    Point b = point(-8, 7, 3);
    assert(point_eq(a, b));
    
}

void test_matrix_translate_not_affect_vectors(bool debug)
{
    Matrix transform = matrix_translate(5, -3, 2);
    Matrix inverse = matrix_inverse(&transform);
    Vector v = vector(-3, 4, 5);
    Vector w = matrix_mul_tuple(&inverse, v);
    assert(vector_eq(v, w));
    
}

void test_reflection_is_scaling_by_negative_value(bool debug)
{
    Matrix transform = matrix_scale(-1, 1, 1);
    Point p = point(2, 3, 5);
    assert(point_eq(matrix_mul_tuple(&transform, p), point(-2, 3, 5)));
    
}

void test_rotate_point_x_axis(bool debug)
{
    Point p = point(0, 1, 0);
    Matrix half_quarter = matrix_rotatex(PI / 4.0f);
    Matrix full_quarter = matrix_rotatex(PI / 2.0f);
    assert(point_eq(matrix_mul_tuple(&half_quarter, p), point(0, sqrtf(2.0f) / 2.0f, sqrtf(2.0f) / 2.f)));
    assert(point_eq(matrix_mul_tuple(&full_quarter, p), point(0, 0, 1)));
    
}

void test_rotate_point_y_axis(bool debug)
{
    Point p = point(0, 0, 1);
    Matrix half_quarter = matrix_rotatey(PI / 4.0f);
    Matrix full_quarter = matrix_rotatey(PI / 2.0f);
    float d = sqrtf(2.0f) / 2.0f;
    assert(point_eq(matrix_mul_tuple(&half_quarter, p), point(d, 0, d)));
    assert(point_eq(matrix_mul_tuple(&full_quarter, p), point(1, 0, 0)));
    
}

void test_rotate_point_z_axis(bool debug)
{
    Point p = point(0, 1, 0);
    Matrix half_quarter = matrix_rotatez(PI / 4.0f);
    Matrix full_quarter = matrix_rotatez(PI / 2.0f);
    float d = sqrtf(2.0f) / 2.0f;
    assert(point_eq(matrix_mul_tuple(&half_quarter, p), point(-d, d, 0)));
    assert(point_eq(matrix_mul_tuple(&full_quarter, p), point(-1, 0, 0)));
    
}

void test_shear_moves_x_proportion_to_y(bool debug)
{
    Matrix transform = matrix_shear(1, 0, 0, 0, 0, 0);
    Point p = point(5, 6, 7);
    assert(point_eq(matrix_mul_tuple(&transform, p), point(11, 6, 7)));
    
}

void test_shear_moves_x_proportion_to_z(bool debug)
{
    Matrix transform = matrix_shear(0, 1, 0, 0, 0, 0);
    Point p = point(5, 6, 7);
    Point p1 = matrix_mul_tuple(&transform, p);
    assert(point_eq(p1, point(12, 6, 7)));
    
}

void test_shear_moves_y_proportion_to_x(bool debug)
{
    Matrix transform = matrix_shear(0, 0, 1, 0, 0, 0);
    Point p = point(5, 6, 7);
    Point p1 = matrix_mul_tuple(&transform, p);
    assert(point_eq(p1, point(5, 11, 7)));
    
}

void test_shear_moves_y_proportion_to_z(bool debug)
{
    Matrix transform = matrix_shear(0, 0, 0, 1, 0, 0);
    Point p = point(5, 6, 7);
    Point p1 = matrix_mul_tuple(&transform, p);
    assert(point_eq(p1, point(5, 13, 7)));
    
}

void test_shear_moves_z_proportion_to_x(bool debug)
{
    Matrix transform = matrix_shear(0, 0, 0, 0, 1, 0);
    Point p = point(5, 6, 7);
    Point p1 = matrix_mul_tuple(&transform, p);
    assert(point_eq(p1, point(5, 6, 12)));
    
}

void test_shear_moves_z_proportion_to_y(bool debug)
{
    Matrix transform = matrix_shear(0, 0, 0, 0, 0, 1);
    Point p = point(5, 6, 7);
    Point p1 = matrix_mul_tuple(&transform, p);
    assert(point_eq(p1, point(5, 6, 13)));
    
}

void test_individual_transformations_applied_in_seq(bool debug)
{
    Point p = point(1, 0, 1);
    Matrix A = matrix_rotatex(PI / 2);
    Matrix B = matrix_scale(5, 5, 5);
    Matrix C = matrix_translate(10, 5, 7);

    Point p2 = matrix_mul_tuple(&A, p);
    assert(point_eq(p2, point(1, -1, 0)));

    Point p3 = matrix_mul_tuple(&B, p2);
    assert(point_eq(p3, point(5, -5, 0)));

    Point p4 = matrix_mul_tuple(&C, p3);
    assert(point_eq(p4, point(15, 0, 7)));

    
}

void test_chained_transformations_applied_in_reverse_order(bool debug)
{
    Point p = point(1, 0, 1);
    Matrix A = matrix_rotatex(PI / 2);
    Matrix B = matrix_scale(5, 5, 5);
    Matrix C = matrix_translate(10, 5, 7);
    Matrix BA = matrix_mul(&B, &A);
    Matrix T = matrix_mul(&C, &BA);
    assert(point_eq(matrix_mul_tuple(&T, p), point(15, 0, 7)));

    
}

void test_creating_querying_ray(bool debug)
{
    Point origin = point(1, 2, 3);
    Vector direction = vector(4, 5, 6);
    Ray r = ray(origin, direction);
    assert(point_eq(r.origin, origin));
    assert(vector_eq(r.direction, direction));
    
}

void test_compute_point_from_distance(bool debug)
{
    Ray r = ray(point(2, 3, 4), vector(1, 0, 0));
    assert(point_eq(ray_position(r, 0), point(2, 3, 4)));
    assert(point_eq(ray_position(r, 1), point(3, 3, 4)));
    assert(point_eq(ray_position(r, -1), point(1, 3, 4)));
    assert(point_eq(ray_position(r, 2.5), point(4.5, 3, 4)));
    
}

void test_ray_intersect_two_points(bool debug)
{
    Ray r = ray(point(0, 0, -5), vector(0, 0, 1));
    Sphere s = sphere();
    Intersections hi = ray_intersect(s, r);
    assert(hi.hitCount == 2);
    assert(flt_eq(hi.intersection[0].t, 4.0f));
    assert(flt_eq(hi.intersection[1].t, 6.0f));
    
}

void test_ray_intersects_at_tangent(bool debug)
{
    Ray r = ray(point(0, 1, -5), vector(0, 0, 1));
    Sphere s = sphere();
    Intersections hi = ray_intersect(s, r);
    assert(hi.hitCount == 2);
    assert(flt_eq(hi.intersection[0].t, 5.0f));
    assert(flt_eq(hi.intersection[1].t, 5.0f));
    
}

void test_ray_misses_sphere(bool debug)
{
    Ray r = ray(point(0, 2, -5), vector(0, 0, 1));
    Sphere s = sphere();
    Intersections hi = ray_intersect(s, r);
    assert(hi.hitCount == 0);
    
}

void test_ray_origin_inside_sphere(bool debug)
{
    Ray r = ray(point(0, 0, 0), vector(0, 0, 1));
    Sphere s = sphere();
    Intersections hi = ray_intersect(s, r);
    assert(hi.hitCount == 2);
    assert(flt_eq(hi.intersection[0].t, -1.0f));
    assert(flt_eq(hi.intersection[1].t, 1.0f));
    
}

void test_ray_sphere_is_behind_ray(bool debug)
{
    Ray r = ray(point(0, 0, 5), vector(0, 0, 1));
    Sphere s = sphere();
    Intersections hi = ray_intersect(s, r);
    assert(hi.hitCount == 2);
    assert(flt_eq(hi.intersection[0].t, -6.0f));
    assert(flt_eq(hi.intersection[1].t, -4.0f));
    
}

void test_ray_intersection_datastructure(bool debug)
{
    Sphere s = sphere();
    Intersection i = intersection(3.5, s);
    assert(flt_eq(i.t, 3.5));
    assert(sphere_eq(i.object, s));
    
}

void test_ray_aggregating_intersections(bool debug)
{
    Sphere s = sphere();
    Intersection i1 = intersection(1, s);
    Intersection i2 = intersection(2, s);
    Intersections hi = intersections(2, i1, i2);
    assert(hi.hitCount == 2);
    assert(hi.intersection[0].t == 1);
    assert(hi.intersection[1].t == 2);
    
}

void test_ray_intersect_sets_object(bool debug)
{
    Ray r = ray(point(0, 0, -5), vector(0, 0, 1));
    Sphere s = sphere();
    Intersections hi = ray_intersect(s, r);
    assert(hi.hitCount == 2);
    assert(sphere_eq(hi.intersection[0].object, s));
    assert(sphere_eq(hi.intersection[1].object, s));
    
}

void test_ray_ray_hit_pos_t(bool debug)
{
    Sphere s = sphere();
    Intersection i1 = intersection(1, s);
    Intersection i2 = intersection(2, s);
    Intersections iss = intersections(2, i1, i2);
    assert(iss.hitCount == 2);
    assert(intersection_eq(ray_hit(iss), i1));
    
}

void test_ray_ray_hit_neg_t(bool debug)
{
    Sphere s = sphere();
    Intersection i1 = intersection(-1, s);
    Intersection i2 = intersection(1, s);
    Intersections iss = intersections(2, i1, i2);
    assert(iss.hitCount == 2);
    assert(intersection_eq(ray_hit(iss), i2));
    
}

void test_ray_ray_hit_both_neg_t(bool debug)
{
    Sphere s = sphere();
    Intersection i1 = intersection(-2, s);
    Intersection i2 = intersection(-1, s);
    Intersections iss = intersections(2, i1, i2);
    assert(iss.hitCount == 2);
    Intersection hitOpt = ray_hit(iss);
    assert(intersection_eq(hitOpt, NO_HIT));
    
}


void test_ray_hit_always_lowest_intersection(bool debug)
{
    Sphere s = sphere();
    Intersection i1 = intersection(5, s);
    Intersection i2 = intersection(7, s);
    Intersection i3 = intersection(-3, s);
    Intersection i4 = intersection(2, s);
    Intersections iss = intersections(4, i1, i2, i3, i4);
    assert(iss.hitCount == 4);
    Intersection hit = ray_hit(iss);
    assert(intersection_eq(hit, i4));
    
}

void test_ray_translation(bool debug)
{
    Ray r = ray(point(1,2,3), vector(0,1,0));
    Matrix translation = matrix_translate(3, 4, 5);
    Ray rayTranslated = ray_transform(r, translation);
    assert(point_eq(rayTranslated.origin, point(4,6,8)));
    assert(vector_eq(rayTranslated.direction, vector(0,1,0)));
}

void test_ray_scaling(bool debug)
{
    Ray r = ray(point(1,2,3), vector(0,1,0));
    Matrix scale = matrix_scale(2, 3, 4);
    Ray rayTranslated = ray_transform(r, scale);
    assert(point_eq(rayTranslated.origin, point(2,6,12)));
    assert(vector_eq(rayTranslated.direction, vector(0,3,0)));
}

void test_new_sphere_identity(bool debug)
{
    Sphere s = sphere();
    Matrix id = matrix_identity(4,4);
    assert(matrix_eq(&s.transform, &id));
}

void test_change_sphere_transformation(bool debug)
{
    Sphere s = sphere();
    Matrix translation = matrix_translate(5,6,7);
    s.transform = translation;
    assert(matrix_eq(&s.transform, &translation));
}

void test_intersect_scaled_sphere(bool debug)
{
    Ray r = ray(point(0,0,-5), vector(0,0,1));
    Sphere s = sphere();
    s.transform = matrix_scale(2,2,2);
    Intersections iss = ray_intersect(s, r);
    assert(iss.hitCount == 2);
    assert(flt_eq(iss.intersection[0].t, 3));
    assert(flt_eq(iss.intersection[1].t, 7));
}


bool run_tests(bool debug)
{
    test_matrix_mul(debug);
    test_matrix_mul_tuple(debug);
    test_matrix_transpose(debug);
    test_matrix_transpose_identity(debug);
    test_matrix_determinant(debug);
    test_matrix_submatrix(debug);
    test_minor(debug);
    test_cofactor(debug);
    test_cofactor_and_determinant(debug);
    test_invertible(debug);
    test_inverse(debug);
    test_matrix_mul_product_by_its_inverse(debug);
    test_matrix_translate_point(debug);
    test_matrix_inversion_translate(debug);
    test_matrix_translate_not_affect_vectors(debug);
    test_reflection_is_scaling_by_negative_value(debug);
    test_rotate_point_x_axis(debug);
    test_rotate_point_y_axis(debug);
    test_rotate_point_z_axis(debug);
    test_shear_moves_x_proportion_to_y(debug);
    test_shear_moves_x_proportion_to_z(debug);
    test_shear_moves_y_proportion_to_x(debug);
    test_shear_moves_y_proportion_to_z(debug);
    test_shear_moves_z_proportion_to_x(debug);
    test_shear_moves_z_proportion_to_y(debug);
    test_individual_transformations_applied_in_seq(debug);
    test_chained_transformations_applied_in_reverse_order(debug);
    test_creating_querying_ray(debug);
    test_compute_point_from_distance(debug);
    test_ray_intersect_two_points(debug);
    test_ray_intersects_at_tangent(debug);
    test_ray_misses_sphere(debug);
    test_ray_origin_inside_sphere(debug);
    test_ray_sphere_is_behind_ray(debug);
    test_ray_intersection_datastructure(debug);
    test_ray_aggregating_intersections(debug);
    test_ray_ray_hit_pos_t(debug);
    test_ray_ray_hit_neg_t(debug);
    test_ray_ray_hit_both_neg_t(debug);
    test_ray_hit_always_lowest_intersection(debug);
    test_ray_translation(debug);
    test_ray_scaling(debug);
    test_new_sphere_identity(debug);
    test_change_sphere_transformation(debug);
    test_intersect_scaled_sphere(debug);

    printf("all tests passed successfully\n");
    return true;
}
